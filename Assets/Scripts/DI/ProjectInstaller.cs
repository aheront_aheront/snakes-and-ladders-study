using Assets.Scripts.Game;
using Zenject;
using Assets.Scripts.MainMenu;

namespace Assets.Scripts.DI
{
    public class ProjectInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<IGameConfiguration>().FromMethod(() => GameConfiguration.Instance);
            Container.Bind<IGameConfigurable>().FromMethod(() => GameConfiguration.Instance);
        }
    }
}