using Assets.Scripts.Game.UI.Player;
using Zenject;

namespace Assets.Scripts.Game.DI
{
    public class GameInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<PlayerChipFactory>().AsSingle();
        }
    }
}