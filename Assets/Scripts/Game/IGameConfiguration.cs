using Assets.Scripts.Models;
using System.Collections.Generic;

namespace Assets.Scripts.Game
{
    public interface IGameConfiguration
    {
        public int GetPlayersCount();
        
        public List<PlayerColor> GetPlayersColor();

    }
}