using System;
using UnityEngine;

namespace Assets.Scripts.Game.Simulation.GameCube
{
    public class GameCube
    {
        public static event Action<int> CubeThrown;
        public static GameCube Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new GameCube();
                return _instance;
            }
        }

        private static GameCube _instance;

        public void ThrowCube()
        {
            int res = UnityEngine.Random.Range(1, 7);
            CubeThrown?.Invoke(res);
        }
    }
}