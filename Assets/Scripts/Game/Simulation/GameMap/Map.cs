using Assets.Scripts.Game.Simulation.GameCube;
using Assets.Scripts.Game.Simulation.Turn;
using System;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Game.Simulation.GameMap
{

    public class Map : MonoBehaviour
    {
        public static event Action PlayersPositionsChanged;
        private const int CELLS_COUNT = 100;

        [Inject] private IGameConfiguration _gameConfiguration;
        private static List<int> _playerChipsPosition = new List<int>();
        
        public static List<int> GetPlayersPositions()
        {
            return _playerChipsPosition;
        }

        private void Awake()
        {
            if(_playerChipsPosition.Count == 0)
            {
                _playerChipsPosition = new List<int>(_gameConfiguration.GetPlayersCount());
                for(int i = 0; i < _gameConfiguration.GetPlayersCount(); i++)
                {
                    _playerChipsPosition.Add(0);
                }
            }
            PlayersPositionsChanged?.Invoke();
        }
        public void MovePlayer(int playerId, int steps)
        {
            _playerChipsPosition[playerId] = Mathf.Clamp(_playerChipsPosition[playerId] + steps, 0, CELLS_COUNT-1);
            while (TransitionController.GetTransitionResult(_playerChipsPosition[playerId]) != _playerChipsPosition[_playerChipsPosition.Count - 1])
            {
                _playerChipsPosition[playerId] = TransitionController.GetTransitionResult(_playerChipsPosition[playerId]);
            }
            PlayersPositionsChanged?.Invoke();
        }


        private void OnEnable()
        {
            GameCube.GameCube.CubeThrown += OnCubeThrown;
        }

        private void OnCubeThrown(int cubeNumber)
        {
            int currentPlayer = TurnKeeper.GetCurrentPlayerId();
            MovePlayer(currentPlayer, cubeNumber);
        }

        private void OnDisable()
        {
            GameCube.GameCube.CubeThrown -= OnCubeThrown;
        }
    }
}