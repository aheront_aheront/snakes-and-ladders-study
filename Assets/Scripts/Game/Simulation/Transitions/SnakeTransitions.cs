using UnityEngine;

public class SnakeTransitions : MonoBehaviour
{
    [SerializeField] private Transition[] transitions;

    private static Transition[] _transitions;
    public static Transition[] Transitions => _transitions;

    private void Awake()
    {
        _transitions = transitions;
    }
}
