using System;

[Serializable]
public struct Transition 
{
    public int startCellId;
    public int endCellId;
}