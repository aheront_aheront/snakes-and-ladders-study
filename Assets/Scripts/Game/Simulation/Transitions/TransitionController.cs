using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TransitionController
{
    private static List<Transition> transitions = new List<Transition>();

    public static int GetTransitionResult(int startCell)
    {
        transitions = SnakeTransitions.Transitions.ToList();
        transitions.AddRange(LadderTransitions.Transitions.ToList());
        if (transitions.Any(t => t.startCellId == startCell))
        {
            return transitions.First(t => t.startCellId == startCell).endCellId;
        }
        else
        {
            return startCell;
        }
    }
}
