using System;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Game.Simulation.Turn
{
    public class TurnKeeper : MonoBehaviour
    {
        public static event Action<int> TurnSwitched;
        
        private static TurnKeeper _instance;

        [Inject] private IGameConfiguration _config;
        private static int _currentTurnPlayerId = 0;

        private void Awake()
        {
            TurnSwitched?.Invoke(_currentTurnPlayerId);
            if (_instance == null)
                _instance = this;
            else
                Destroy(gameObject);
        }

        public static int GetCurrentPlayerId()
        {
            return _currentTurnPlayerId;
        }

        public void SwitchTurn()
        {
            _currentTurnPlayerId = (_currentTurnPlayerId + 1) % _config.GetPlayersCount();
            TurnSwitched?.Invoke(_currentTurnPlayerId);
        }
    }
}