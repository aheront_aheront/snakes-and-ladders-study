using UnityEngine;

namespace Assets.Scripts.Game.Simulation.Turn
{
    [RequireComponent(typeof(TurnKeeper))]
    public class TurnSwitcher : MonoBehaviour
    {
        private TurnKeeper _turnKeeper;

        private const float TIME_TO_NEXT_TURN = 2f;
        private float timeToTurnSwitch = 0f;

        private void Awake()
        {
            TryGetComponent(out _turnKeeper);    
        }

        private void OnEnable()
        {
            GameCube.GameCube.CubeThrown += ResetTimer;
        }

        private void ResetTimer(int sideNumber)
        {
            timeToTurnSwitch = TIME_TO_NEXT_TURN;
        }

        private void FixedUpdate()
        {
            if (timeToTurnSwitch == 0) return;
            timeToTurnSwitch -= Time.fixedDeltaTime;
            if (timeToTurnSwitch <= 0)
            {
                _turnKeeper.SwitchTurn();
                timeToTurnSwitch = 0;
            }
        }

        private void OnDisable()
        {
            GameCube.GameCube.CubeThrown -= ResetTimer;
        }
    }
}