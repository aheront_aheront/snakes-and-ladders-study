using Assets.Scripts.Game.Simulation.Turn;
using Assets.Scripts.Utils;
using TMPro;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Game.UI
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class CurrentPlayerText : MonoBehaviour
    {
        private TextMeshProUGUI _text;
        [Inject] private IGameConfiguration _config;

        private void Awake()
        {
            TryGetComponent(out _text);
        }

        private void OnEnable()
        {
            TurnKeeper.TurnSwitched += UpdateText;
            UpdateText(TurnKeeper.GetCurrentPlayerId());
        }

        private void UpdateText(int currentPlayerId)
        {
            _text.text = $"Current\nPlayer: {currentPlayerId + 1}";
            var currentPlayerColor = _config.GetPlayersColor()[currentPlayerId];
            _text.color = PlayerColorUtils.GetUnityColorByPlayerColor(currentPlayerColor);
        }

        private void OnDisable()
        {
            TurnKeeper.TurnSwitched -= UpdateText;
        }
    }
}