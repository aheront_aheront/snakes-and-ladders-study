using Assets.Scripts.Game.Simulation.GameMap;
using Assets.Scripts.Game.UI.Player;
using JetBrains.Annotations;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Game.UI.MapUI
{
    [RequireComponent(typeof(PlayerChipsController))]
    public class MapPresentator : MonoBehaviour
    {
        [SerializeField] private Transform _firstCellPosition;
        [SerializeField] private Vector2 _cellSize;

        private PlayerChipsController _playerChipsController;

        private const int CELLS_COUNT = 100;
        private const int CELLS_IN_ROW = 10;
        private readonly Vector2 PLAYER_OVERLAY_SHIFT = new Vector2(0.25f, 0.25f);
        
        private List<Vector2> _cellsPositions;

        public Vector2 GetCellPosition(int index)
        {
            return _cellsPositions[index];
        }

        void Awake()
        {
            TryGetComponent(out _playerChipsController);

            _cellsPositions = new List<Vector2>(CELLS_COUNT);
            bool goingToTheRight = true;
            int cellShiftX = 0;
            int cellShiftY = 0;

            for (int i = 1; i <= CELLS_COUNT; i++)
            {

                _cellsPositions.Add(new Vector2(_firstCellPosition.position.x + _cellSize.x * cellShiftX,
                        _firstCellPosition.position.y + _cellSize.y * cellShiftY));
                if (i % CELLS_IN_ROW == 0)
                {
                    cellShiftY++;
                    goingToTheRight = !goingToTheRight;
                    continue;
                }
                if (goingToTheRight)
                {
                    cellShiftX++;
                }
                else
                {
                    cellShiftX--;
                }
            }
        }

        private void OnEnable()
        {
            Map.PlayersPositionsChanged += RefreshPlayersPositions; // TODO add map change event
            RefreshPlayersPositions();
        }

        private void RefreshPlayersPositions()
        {
            var currentPlayersPositions = Map.GetPlayersPositions();
            var playersChips = _playerChipsController.GetPlayersChips();
            Dictionary<int,int> playerOnCellCount = new Dictionary<int,int>();
            for (int i = 0; i < currentPlayersPositions.Count; i++)
            {
                int playerCellIndex = currentPlayersPositions[i];
                Vector3 playerPosition = _cellsPositions[playerCellIndex];
                if (playerOnCellCount.ContainsKey(playerCellIndex))
                {
                    playerPosition.x += PLAYER_OVERLAY_SHIFT.x * playerOnCellCount.GetValueOrDefault(playerCellIndex);
                    playerPosition.y += PLAYER_OVERLAY_SHIFT.y * playerOnCellCount.GetValueOrDefault(playerCellIndex);
                    playerPosition.z = playerOnCellCount.GetValueOrDefault(playerCellIndex);
                    playerOnCellCount[playerCellIndex]++;
                }
                else
                {
                    playerOnCellCount[playerCellIndex] = 1;
                }
                playersChips[i].SetPosition(playerPosition);
            }
        }

        private void OnDisable()
        {
            Map.PlayersPositionsChanged -= RefreshPlayersPositions;
        }
    }
}