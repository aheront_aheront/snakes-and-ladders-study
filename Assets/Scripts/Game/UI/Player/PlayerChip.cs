using Assets.Scripts.Models;
using UnityEngine;

namespace Assets.Scripts.Game.UI.Player
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class PlayerChip : MonoBehaviour
    {
        private SpriteRenderer _renderer;

        private void Awake()
        {
            TryGetComponent(out _renderer);
        }


        public void SetChipColor(PlayerColor color)
        {
            string path = "Sprites/Players/";
            switch (color)
            {
                case PlayerColor.Red:
                    path += "Red";
                    break;
                case PlayerColor.Yellow:
                    path += "Yellow";
                    break;
                case PlayerColor.Blue:
                    path += "Blue";
                    break;
                case PlayerColor.Purple:
                    path += "Purple";
                    break;
            }
            _renderer.sprite = Resources.Load<Sprite>(path);
        }

        public void SetPosition(Vector3 position)
        {
            transform.position = position;
        }
    }
}