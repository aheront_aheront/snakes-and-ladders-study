using Assets.Scripts.Models;
using System;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Game.UI.Player
{
    public class PlayerChipFactory
    {
        private GameObject _prefab;
        public PlayerChipFactory() {
            _prefab = Resources.Load<GameObject>("Prefabs/Game/PlayerChip");
        }

        public PlayerChip SpawnChip(PlayerColor chipColor, Transform parent)
        {
            var chip = UnityEngine.Object.Instantiate(_prefab, parent).GetComponent<PlayerChip>();
            chip.SetChipColor(chipColor);
            return chip;
        }
    }
}