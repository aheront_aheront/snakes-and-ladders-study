using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Game.UI.Player
{
    public class PlayerChipsController : MonoBehaviour
    {
        [Inject] private IGameConfiguration _config;
        [Inject] private PlayerChipFactory _chipFactory;

        private List<PlayerChip> _playerChips = new();

        private void Awake()
        {
            //order is important
            for (int i = 0; i < _config.GetPlayersCount(); i++)
            {
                _playerChips.Add(_chipFactory.SpawnChip(_config.GetPlayersColor()[i], transform));
            }
        }

        public List<PlayerChip> GetPlayersChips()
        {
            return _playerChips;
        }
    }
}