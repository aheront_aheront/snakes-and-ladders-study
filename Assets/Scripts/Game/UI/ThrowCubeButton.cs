using Assets.Scripts.Game.Simulation.GameCube;
using Assets.Scripts.Game.Simulation.Turn;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Game.UI
{
    [RequireComponent(typeof(Button))]
    public class ThrowCubeButton : MonoBehaviour
    {
        private Button _button;

        private void Awake()
        {
            TryGetComponent(out _button);
        }

        private void OnEnable()
        {
            _button.onClick.AddListener(ThrowCube);
            TurnKeeper.TurnSwitched += TurnOnButton;
        }

        private void ThrowCube()
        {
            GameCube.Instance.ThrowCube();
            _button.enabled = false;
        }

        private void TurnOnButton(int currentPlayerId)
        {
            _button.enabled = true;
        }

        private void OnDisable()
        {
            _button.onClick?.RemoveListener(ThrowCube);
            TurnKeeper.TurnSwitched -= TurnOnButton;
        }
    }
}