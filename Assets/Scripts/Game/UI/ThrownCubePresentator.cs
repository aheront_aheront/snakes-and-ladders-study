using Assets.Scripts.Game.Simulation.GameCube;
using Assets.Scripts.Game.Simulation.Turn;
using UnityEngine;

namespace Assets.Scripts.Game.UI
{
    public class ThrownCubePresentator : MonoBehaviour
    {
        [SerializeField] private GameObject _cubeModel;
        [SerializeField] private Vector3[] cubeSidesEulers;

        private void Awake()
        {
            _cubeModel.SetActive(false);
        }

        private void OnEnable()
        {
            GameCube.CubeThrown += OnCubeThrown;
            TurnKeeper.TurnSwitched += HideCube;
        }

        private void OnCubeThrown(int number)
        {
            _cubeModel.transform.eulerAngles = cubeSidesEulers[number - 1];
            _cubeModel.SetActive(true);
        }

        private void HideCube(int _)
        {
            _cubeModel.SetActive(false);
        }

        private void OnDisable()
        {
            GameCube.CubeThrown -= OnCubeThrown;
            TurnKeeper.TurnSwitched -= HideCube;
        }
    }
}