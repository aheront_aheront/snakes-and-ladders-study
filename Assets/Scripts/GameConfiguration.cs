using Assets.Scripts.Game;
using Assets.Scripts.MainMenu;
using Assets.Scripts.Models;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

namespace Assets.Scripts
{
    public class GameConfiguration : IGameConfigurable, IGameConfiguration
    {
        public static GameConfiguration Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new GameConfiguration();
                return _instance;
            }
        }

        private static GameConfiguration _instance;

        private int _playersCount;
        private List<PlayerColor> _playersColors;

        private GameConfiguration(){}

        public void SetPlayersCount(int count)
        {
            if (this == Instance)
            {
                _playersCount = count;
            }
            else
            {
                Instance.SetPlayersCount(count);
            }
        }

        public void SetPlayersColors(List<PlayerColor> colors)
        {
            if (this == Instance)
            {
                _playersColors = colors;
            }
            else
            {
                Instance.SetPlayersColors(colors);
            }
        }

        public int GetPlayersCount()
        {
            if (this == Instance)
            {
                return _playersCount;
            }
            else
            {
                return Instance.GetPlayersCount();
            }
        }

        public List<PlayerColor> GetPlayersColor()
        {
            if (this == Instance) 
            { 
                return _playersColors;
            }
            else
            {
                return Instance.GetPlayersColor();
            }    
        }
    }
}