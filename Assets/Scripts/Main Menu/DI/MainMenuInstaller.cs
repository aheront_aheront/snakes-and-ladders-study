using Assets.Scripts.MainMenu.PlayersColors.ColorSelector;
using Assets.Scripts.MainMenu.PlayersCount.Counter;
using Zenject;

namespace Assets.Scripts.MainMenu.DI
{
    public class MainMenuInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<UIController>().AsSingle();
            Container.Bind<IGameConfigurable>().To<GameConfiguration>().AsSingle();

            Container.Bind<PlayersCountCounter>().AsTransient();

            Container.Bind<PlayerColorSelector>().AsTransient();
        }
    }
}