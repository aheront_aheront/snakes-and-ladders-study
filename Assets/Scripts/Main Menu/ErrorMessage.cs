using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MainMenu
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class ErrorMessage : MonoBehaviour
    {
        [Inject] private UIController _controller;

        private TextMeshProUGUI _text;

        public void Show(string message)
        {
            _text.text = message;
        }

        private void Awake()
        {
            TryGetComponent(out _text);
            _text.text = "";
        }

        private void OnEnable()
        {
            _controller.AttachErrorMessage(this);
        }

        private void OnDisable()
        {
            _controller.DetachErrorMessage();
        }
    }
}