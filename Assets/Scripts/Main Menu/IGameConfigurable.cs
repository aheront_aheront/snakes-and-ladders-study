using Assets.Scripts.Models;
using System.Collections.Generic;

namespace Assets.Scripts.MainMenu
{
    public interface IGameConfigurable
    {
        public void SetPlayersCount(int count);
        public void SetPlayersColors(List<PlayerColor> colors);

    }
}