using Assets.Scripts.Models;
using Assets.Scripts.Utils;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Assets.Scripts.MainMenu.PlayersColors.ColorSelector
{
    public class PlayerColorPresentator : MonoBehaviour
    {
        [Inject] private PlayerColorSelector _selector;

        [SerializeField] private Button _nextButton;
        [SerializeField] private Image _colorImage;
        [SerializeField] private Button _prevButton;

        public PlayerColor GetColor()
        {
            return _selector.CurrentPlayerColor;
        }

        public void SetVisible(bool isVisible)
        {
            gameObject.SetActive(isVisible);
        }

        private void OnEnable()
        {
            _selector.PlayerColorChanged += UpdatePlayerColor;
            UpdatePlayerColor();
            _nextButton.onClick.AddListener(NextColor);
            _prevButton.onClick.AddListener(PreviousColor);
        }

        private void UpdatePlayerColor()
        {
            var newColor = _selector.CurrentPlayerColor;
            _colorImage.color = PlayerColorUtils.GetUnityColorByPlayerColor(newColor);
        }

        private void NextColor()
        {
            _selector.NextColor();
        }

        private void PreviousColor()
        {
            _selector.PrevColor();
        }

        private void OnDisable()
        {
            _selector.PlayerColorChanged -= UpdatePlayerColor;
            _nextButton.onClick.RemoveListener(NextColor);
            _prevButton.onClick.RemoveListener(PreviousColor);
        }
    }
}