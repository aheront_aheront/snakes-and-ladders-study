using Assets.Scripts.Models;
using System;
using UnityEngine;

namespace Assets.Scripts.MainMenu.PlayersColors.ColorSelector
{
    public class PlayerColorSelector
    {
        public event Action PlayerColorChanged;
        public PlayerColor CurrentPlayerColor
        {
            get => (PlayerColor)_playerColors.GetValue(_playerColorId);
        }

        [SerializeField] private int playerId;
        private int _playerColorId = 0;
        private Array _playerColors = Enum.GetValues(typeof(PlayerColor));

        public void NextColor()
        {
            _playerColorId = (_playerColorId + 1) % _playerColors.Length;
            PlayerColorChanged?.Invoke();
        }

        public void PrevColor()
        {
            _playerColorId = (_playerColorId + _playerColors.Length - 1) % _playerColors.Length;
            PlayerColorChanged?.Invoke();
        }
    }
}