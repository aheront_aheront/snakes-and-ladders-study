using Assets.Scripts.MainMenu.PlayersColors.ColorSelector;
using Assets.Scripts.MainMenu.PlayersCount;
using Assets.Scripts.Models;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MainMenu.PlayersColors
{
    public class PlayersColors : MonoBehaviour
    {
        [Inject] private UIController _controller;

        [SerializeField] private PlayersCounter _playersCounter;
        [SerializeField] private List<PlayerColorPresentator> _selectors;

        public List<PlayerColor> GetPlayersColors()
        {
            List<PlayerColor> resut = new List<PlayerColor>();
            for (int i = 0; i < _playersCounter.GetPlayersCount(); i++)
            {
                resut.Add(_selectors[i].GetColor());
            }
            return resut;
        }

        private void OnEnable()
        {
            _controller.AttachPlayersColors(this);
            _playersCounter.PlayerCountChanged += UpdateActiveSelectors;
            UpdateActiveSelectors();
        }

        private void UpdateActiveSelectors()
        {
            int playersCount = _playersCounter.GetPlayersCount();
            for(int i = 0; i < _selectors.Count; i++)
            {
                _selectors[i].SetVisible(i < playersCount);
            }
        }

        private void OnDisable()
        {
            _controller.DetachPlayersColors();
        }
    }
}