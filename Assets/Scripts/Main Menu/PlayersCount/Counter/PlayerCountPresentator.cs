using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Assets.Scripts.MainMenu.PlayersCount.Counter
{
    public class PlayerCountPresentator : MonoBehaviour
    {
        public event Action PlayerCountChanged;
        [Inject] private PlayersCountCounter _counter;

        [SerializeField] private Button _increaseButton;
        [SerializeField] private TextMeshProUGUI _text;
        [SerializeField] private Button _decreaseButton;

        public int GetPlayersCount()
        {
            return _counter.CurrentPlayersCount;
        }

        private void OnEnable()
        {
            _counter.PlayersCountChanged += UpdatePlayersCount;
            UpdatePlayersCount();
            _increaseButton.onClick.AddListener(Increase);
            _decreaseButton.onClick.AddListener(Decrease);
        }

        private void UpdatePlayersCount()
        {
            _text.text = _counter.CurrentPlayersCount.ToString();
            PlayerCountChanged?.Invoke();
        }

        private void Increase()
        {
            _counter.Increment();
        }

        private void Decrease()
        {
            _counter.Decrement();
        }

        private void OnDisable()
        {
            _counter.PlayersCountChanged -= UpdatePlayersCount;
            _increaseButton.onClick.RemoveListener(Increase);
            _decreaseButton.onClick.RemoveListener(Decrease);
        }
    }
}