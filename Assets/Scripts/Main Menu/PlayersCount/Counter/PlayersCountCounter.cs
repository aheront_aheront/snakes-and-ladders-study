using System;
using UnityEngine;

namespace Assets.Scripts.MainMenu.PlayersCount.Counter {
    public class PlayersCountCounter
    {
        public event Action PlayersCountChanged;
        public int CurrentPlayersCount
        {
            get => _playersCount;
            private set
            {
                value = Mathf.Clamp(value, Models.PlayersCount.MIN_PLAYERS, Models.PlayersCount.MAX_PLAYERS);
                if (_playersCount == value) return;
                _playersCount = value;
                PlayersCountChanged?.Invoke();
            }
        }

        private int _playersCount = Models.PlayersCount.MIN_PLAYERS;

        public void Increment()
        {
            CurrentPlayersCount++;
        }

        public void Decrement()
        {
            CurrentPlayersCount--;
        }
    }
}