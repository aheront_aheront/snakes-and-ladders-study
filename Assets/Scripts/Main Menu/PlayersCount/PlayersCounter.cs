using Assets.Scripts.MainMenu.PlayersCount.Counter;
using System;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MainMenu.PlayersCount
{
    public class PlayersCounter : MonoBehaviour
    {
        public event Action PlayerCountChanged;
        [Inject] private UIController _controller;

        [SerializeField] private PlayerCountPresentator _counter;

        public int GetPlayersCount()
        {
            return _counter.GetPlayersCount();
        }

        private void OnEnable()
        {
            _counter.PlayerCountChanged += OnPlayerCountChanged;
            _controller.AttachPlayersCounter(this);
        }

        private void OnPlayerCountChanged() => PlayerCountChanged?.Invoke();

        private void OnDisable()
        {
            _controller.DetachPlayersCounter();
            _counter.PlayerCountChanged -= OnPlayerCountChanged;
        }
    }
}