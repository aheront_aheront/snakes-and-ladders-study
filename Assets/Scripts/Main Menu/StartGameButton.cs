using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Assets.Scripts.MainMenu {
    [RequireComponent(typeof(Button))]
    public class StartGameButton : MonoBehaviour
    {
        [Inject] private UIController _controller;

        private Button _button;

        private void Awake()
        {
            TryGetComponent(out _button);
        }

        private void OnEnable()
        {
            _controller.AttachStartButton(_button);
        }

        private void OnDisable()
        {
            _controller.DetachStartButton();
        }

    }
}