using Assets.Scripts.MainMenu.PlayersCount;
using Assets.Scripts.Models;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;

namespace Assets.Scripts.MainMenu
{
    public class UIController
    {
        [Inject] IGameConfigurable _gameConfiguration;

        private Button _startGameButton;
        private PlayersCounter _playersCount;
        private PlayersColors.PlayersColors _playersColors;
        private ErrorMessage _errorMessage;

        public void AttachStartButton(Button startGameButton)
        {
            _startGameButton = startGameButton;
            _startGameButton.onClick.AddListener(StartGame);
        }

        public void AttachPlayersCounter(PlayersCounter counter)
        {
            _playersCount = counter;
        }

        public void AttachPlayersColors(PlayersColors.PlayersColors colors)
        {
            _playersColors = colors;
        }

        public void AttachErrorMessage(ErrorMessage errorMessage)
        {
            _errorMessage = errorMessage;
        }

        public int GetPlayersCount()
        {
            return _playersCount.GetPlayersCount();
        }

        public void DetachStartButton()
        {
            _startGameButton.onClick.RemoveListener(StartGame);
            _startGameButton = null;
        }

        public void DetachPlayersCounter()
        {
            _playersCount = null;
        }

        public void DetachPlayersColors()
        {
            _playersColors = null;
        }

        public void DetachErrorMessage()
        {
            _errorMessage = null;
        }

        private void StartGame()
        {
            int playersCount = _playersCount.GetPlayersCount();
            List<PlayerColor> playersColors = _playersColors.GetPlayersColors();
            for (int i = 0; i < playersColors.Count; i++)
            {
                for(int j = i + 1; j < playersColors.Count; j++)
                {
                    if (playersColors[i] == playersColors[j])
                    {
                        _errorMessage.Show("All players must have different colors!");
                        return;
                    }
                }
            }
            _gameConfiguration.SetPlayersCount(playersCount);
            _gameConfiguration.SetPlayersColors(playersColors);
            SceneManager.LoadScene(1, LoadSceneMode.Single);
        }
    }
}