namespace Assets.Scripts.Models
{
    public enum PlayerColor
    {
        Red,
        Yellow,
        Blue,
        Purple,
    }
}