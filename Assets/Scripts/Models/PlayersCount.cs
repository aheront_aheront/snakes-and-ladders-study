namespace Assets.Scripts.Models
{
    public static class PlayersCount
    {
        public const int MAX_PLAYERS = 4;
        public const int MIN_PLAYERS = 1;
    }
}