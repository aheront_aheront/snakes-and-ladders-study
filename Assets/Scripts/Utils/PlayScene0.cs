using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayScene0 : MonoBehaviour
{
    void Start()
    {
        if (Application.isEditor && (GameObject.Find("Scene 0 Object") == null))
        {
            SceneManager.LoadScene(0);
        }
    }
}