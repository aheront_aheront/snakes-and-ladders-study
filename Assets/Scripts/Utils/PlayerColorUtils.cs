using Assets.Scripts.Models;
using UnityEngine;

namespace Assets.Scripts.Utils {
    public static class PlayerColorUtils 
    { 
        public static Color GetUnityColorByPlayerColor(PlayerColor playerColor)
        {
            switch (playerColor)
            {
                case PlayerColor.Blue:
                    return Color.blue;
                case PlayerColor.Purple:
                    return new Color(0.5f, 0, 0.5f);
                case PlayerColor.Red:
                    return Color.red;
                case PlayerColor.Yellow:
                    return Color.yellow;
                default:
                    throw new System.Exception("color not found");
            }
        }
    }
}